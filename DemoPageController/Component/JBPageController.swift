//
//  CustomPageControllerViewController.swift
//  MuseStressMonitor
//
//  Created by Jishnu on 08/12/16.
//  Copyright © 2016 InApp. All rights reserved.
//

import Foundation
import UIKit


@objc protocol JBDelegate {
    @objc optional func pageControllerDidChange(WithCurrentPageIndex index:Int)
}

class JBPageController: UIPageViewController,JBDelegate
{
    var jbDelegate:JBDelegate?
    var contentVCs:[UIViewController]? {
        didSet {
              //setFirstVC()
        }
    }
    
    init(InitWithContentVCs contentVCs:[UIViewController],WithTransitionStyle transitionStyle:UIPageViewControllerTransitionStyle,ForViewController vc:UIViewController ) {
        self.contentVCs = contentVCs
      
        super.init(transitionStyle: transitionStyle, navigationOrientation: .horizontal, options: nil)
        self.view.frame = vc.view.frame
        vc.view.insertSubview((self.view)!, at: 0)
    }
    
    required init?(coder: NSCoder) {
       super.init(coder: coder)
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setDelegateAndDataSource()
        setFirstVC()

        
    }
    
    func setDelegateAndDataSource(){
        dataSource = self
        delegate = self
    }
    
    func setFirstVC(){
        
        
        if let firstViewController =   self.contentVCs?.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: false,
                               completion: nil)
        }
    }
}



// MARK: UIPageViewControllerDataSource

extension JBPageController : UIPageViewControllerDataSource,UIPageViewControllerDelegate {
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        
        
        guard let viewControllerIndex = contentVCs?.index(of: viewController) else {
            return nil
        }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else {
            return nil
        }
        guard (contentVCs?.count)! > previousIndex else {
            return nil
        }
        return contentVCs?[previousIndex]
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        
        guard let viewControllerIndex = contentVCs?.index(of: viewController) else {
            return nil
        }
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = contentVCs?.count
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        guard orderedViewControllersCount! > nextIndex else {
            return nil
        }
        return contentVCs?[nextIndex]
    }
   
    
    
    func setPageControllerView(ForIndex index:Int){
        if let dataSource =  contentVCs{
            let direction = scrollDirection(forIndex: index)
            self.setViewControllers([(contentVCs?[index])!], direction: direction, animated: true, completion: nil)
        }
    }
    
    func scrollDirection(forIndex:Int) -> UIPageViewControllerNavigationDirection {
        var dir : UIPageViewControllerNavigationDirection = .forward
        
        guard let currVC = self.viewControllers?.first else {
            return dir
        }
        guard let curIndex = contentVCs?.index(of: currVC) else {
            return dir
        }
        dir = (forIndex >= curIndex) ? UIPageViewControllerNavigationDirection.forward : UIPageViewControllerNavigationDirection.reverse
        return dir
    }
    
    // MARK: UIPageViewControllerDelegate
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if (!completed)
        {
            return
        }
        if let vcIndex = contentVCs?.index(of: pageViewController.viewControllers!.first!) {
            jbDelegate?.pageControllerDidChange!(WithCurrentPageIndex: vcIndex)
        }
    }
    

}
