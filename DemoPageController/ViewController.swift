//
//  ViewController.swift
//  DemoPageController
//
//  Created by Jishnu on 14/12/16.
//  Copyright © 2016 Jishnu Inc. All rights reserved.
//

import UIKit

class ViewController: UIViewController ,JBDelegate{

    var pageController :JBPageController?
    lazy var contentViewControllers: [UIViewController] = {
        return [ viewControllerForVCID(id: ViewControllerID.vcA),
                 viewControllerForVCID(id: ViewControllerID.vcB),
                 viewControllerForVCID(id: ViewControllerID.vcC)]
    }()
    
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        pageController = JBPageController(InitWithContentVCs: contentViewControllers, WithTransitionStyle: .pageCurl ,ForViewController:self )
        pageController?.jbDelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func pageControllerDidChange(WithCurrentPageIndex index:Int){
        print(index)
    }
   
    @IBAction func button1Selected(_ sender: Any) {
    }
    
    @IBAction func button2Selected(_ sender: Any) {
    }
    
    @IBAction func button3Selected(_ sender: Any) {
    }
}

func viewControllerForVCID(id:ViewControllerID) -> UIViewController {
    return UIStoryboard(name: "Main", bundle: nil) .
        instantiateViewController(withIdentifier:id.rawValue)
}
enum ViewControllerID : String {
    case vcA = "vc1"
    case vcB = "vc2"
    case vcC = "vc3"
   
}
